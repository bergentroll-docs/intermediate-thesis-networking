#!/bin/env python3

import csv
import re

# Открыть файл с базой CVE в CSV-формате.
csv_file = open('allitems.csv', 'r', encoding='cp1251')

# Преобразовать файл в список словарей. Разбор начинается с третьей
# строки, поскольку на данный момент в ней выгрузки содержат
# CSV-заголовок. Ad-hoc решение.
reader = csv.DictReader(csv_file.readlines()[2:])

# Словарь для подсчёта результатов.
result_dict = dict()

# Регулярное выражение соответсвует подстроке 'snmp' независимо от
# регистра, например: 'net-snmp.', 'bsnmpd' 'SNMPv3'.
regex = re.compile('snmp', re.IGNORECASE)

# Проход по словарям CVE. При совпадении описания уязвимости с регулярным
# выражением счётчик для соответствующего года увеличивается.
for row in reader:
    if re.search(regex, row['Description']):
        year = int(row['Name'][4:8])
        if not result_dict.get(year):
            result_dict[year] = 0
        result_dict[year] += 1

# Вывод данных для LaTeX-таблицы.
acc = 0
for pair in sorted(result_dict.items()):
    acc += pair[1]
    print(fr'{pair[0]} & {pair[1]} & {acc} \\')

# Отбивка.
print()

# Вывод данных для PGFPlots-графика.
for pair in sorted(result_dict.items()):
    print(pair, end='\n')

# vi: textwidth=70
